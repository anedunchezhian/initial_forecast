# -*- coding: utf-8 -*-
"""
Created on Mon Jan 11 13:24:56 2021

@author: anedunchezhian
"""

import pandas as pd
import os
import numpy as np
from datetime import datetime as dt

def str_clean(df,cols,force=True):
    if (force==True) :
        for i in cols:
            df[i] = df[i].astype(str).str.strip()
    else:
        for i in cols:
            if df[i].dtypes=='object':
                df[i] = df[i].astype(str).str.strip()
            else:
                pass
            
    return df

def num_clean(df,cols,force=True,fillna=True):
    if (force==True) :
        for i in cols:
            df[i] = pd.to_numeric(df[i],errors='coerce')
            if (fillna==True) : df[i]=df[i].fillna(0) 
                
    else:
        for i in cols:
            if df[i].dtypes=='object':
                df[i] = pd.to_numeric(df[i],errors='coerce')
                if (fillna==True) : df[i]=df[i].fillna(0) 

            else:
                pass
            
    return df

def read_sbom(path = path):
    print('\nReading SBOM...')
    df= pd.read_excel(path+'/Source/R1T SBOM Progress Report.xlsx',sheet_name='SBOM MID Report')
    
    df.columns = df.columns.str.strip()
    
    sbom = df.copy()
    sbom  = str_clean(sbom,cols=['SBOM Level','Classification'])
    # sbom = sbom[sbom['SBOM Level'].isin(['L4'])]
    sbom = sbom[sbom['Classification'].isin(['Service Part','Colored Service Part','Service Assembly','Service Fastener'])]
    sbom = sbom[['SBOM Level','Extracted Part #','IMPLM_Name(R)','IMPLM_Title(R)',
                 'Reused?', 'SBOM System', 'SBOM SubSystem']]
    sbom_final =  sbom.groupby(['Extracted Part #','IMPLM_Name(R)','IMPLM_Title(R)'],as_index=False)['SBOM Level'].count()
    del sbom_final['SBOM Level']
    return sbom_final


def read_ebom(path = path):
    print('\nReading EBOM...')
    df= pd.read_excel(path+'/Source/R1T SBOM Progress Report.xlsx',sheet_name='EBOM')
    df.columns = df.columns.str.strip()
    ebom = df.copy()
    ebom  = str_clean(ebom,cols=['Name','Title', 'SBOM Category', 'SBOM Subcategory'])    
    
    return ebom

def read_full_ebom(path = path):
    print('\nReading Full_BOM...')
    df= pd.read_excel(path+'/Source/R1T SBOM Progress Report.xlsx',sheet_name='FullEBOM')
    df.columns = df.columns.str.strip()
    febom = df.copy()
    febom  = str_clean(febom,cols=['NameCheck','SBOM Check', 'Name','Parent','Title'])
    
    return febom

def read_pfep(path = path):
    print('\nReading PFEP...')
    df= pd.read_csv(path+'/Source/pfep_dataexport_2021-01-11T20_40_09Z.csv')
    df.columns = df.columns.str.strip()
    pfep = df.copy()
    pfep  = str_clean(pfep,cols=pfep.columns,force=False)
    pfep = pfep[pfep['Status']=='Active']    
    return pfep

def read_seg_inputs(path = path,folder = '/Inputs'):
    print('\nReading Failure_Rate_Factor...')
    df = pd.read_excel(path+folder+'/seg_input.xlsx')
    df.columns = df.columns.str.strip()
    df  = str_clean(df,cols=df.columns,force=False)
    return df

def read_we_predict_data(path = path,folder = '/Inputs'):
    print('\nReading We_Predict_data...')
    df = pd.read_csv(path+folder+'/We-Predict-Combined-Data-R2-Raw-Data.csv')
    df.columns = df.columns.str.strip()  
    df = str_clean(df,cols=df.columns,force=False)
    df = num_clean(df,cols=['Service/1000','ProjectedService/1000'])
    df = df[~df['VehType'].isin(['ICE'])]
    df = df[df['Term']=='12MIS']
    x = df.groupby('CategoryName')['Service/1000'].mean().rename('service_mean')
    y = df.groupby('CategoryName')['Service/1000'].min().rename('service_min')
    z = df.groupby('CategoryName')['Service/1000'].max().rename('service_max')
    dd = pd.concat([x,y,z],axis=1).reset_index()
    return dd

def read_vehicle_population(path = path,folder = '/Inputs'):
    print('\nReading Vehical Population...')
    df = pd.read_excel(path+folder+'/Vehical_Preorder_Data.xlsx')
    df.columns = df.columns.str.strip()
    df['Month'] = pd.to_datetime(df['Month'])
    df  = str_clean(df,cols=df.columns,force=False)
    return df


def forecast_basic(veh_parc,dfp,vehicle_model,popu_type=str):
    '''
    Parameters
    ----------
    veh_parc : DataFrame
        Vehical Parc Dataframe.
    dfp : DataFrame
        Input data with Part_s,reliability_rate,collision_rate,service_mean,SBOM Check.
    popu_type : String
        Vehical Population type(Monthly production or Total Onroad Population), both columns are available in the veh_parc DataFrame
    Returns
    -------
    fcst : DataFrame
        Estimated Monthly Forecast, Std. Deviation, 12M_Avg_Fcst.
    '''
    q = veh_parc.set_index('Month').copy()
    q = q[q['Vehicle'].isin([vehicle_model])]
    g = q.copy()
    k = dfp.set_index('Part_p').copy()
    k = k[k['Program'].isin([vehicle_model])]
    
    print("\nCalculating Forecast....")
    fcst = pd.DataFrame(columns = k.index,index = q.index)
    for i in k.index[:]:
        try:
            g = q.copy()
            my_data = k.loc[i,:].copy()
            for j in g.index:
                my_month = g.loc[j,:].copy()
                g.loc[j,'fcst'] = ((my_month[popu_type]*(my_data['reliability_rate']/12)*1.02)  #reliability factor and realiability bias
                                    +(my_month[popu_type]*(my_data['collision_rate']/12)*1.01)   #collision rate and bias
                                    # +(my_month[popu_type]*my_data['service_mean']/12)*1.01      #avg_service_rate per month (estimate from we_predict data- based on similar EV's)
                                    +(my_month[popu_type]*my_data['Qty Per Vehicle']*0.05))      #part per vehicle Bias
                # print(g.loc[j,'fcst'])
            fcst.loc[:,i] = np.round_(g.loc[:,'fcst'],decimals=0).copy()
        except:
            pass
    
    fcst = fcst.T
    fcst.columns = [(x.strftime('%m/%y')+'_FCST') for x in (fcst.columns[:])]
    # fcst = fcst.fillna(0)
    fcst['stdDev'] = np.std(fcst,axis=1)
    fcst['avg_fcst'] = np.mean(fcst.iloc[:,:12],axis=1)
    fcst = fcst.reset_index()
    return fcst


def final_structure():
    
    path = r'C:\Arun\Projects\Forecast\Data_Analysis'
    vehicle_model = 'R1T'

    ##PFEP
    pfep = read_pfep()
    
    my_pfep = pfep[pfep['Program'].isin([vehicle_model])]       #Only Filter for R1T Model parts
    my_pfep.loc[:,'First Created Date'] = pd.to_datetime(my_pfep['First Created Date'])
    my_pfep = my_pfep.sort_values(by=['Part Number','First Created Date'])  #Sorting data
    my_pfep = my_pfep.drop_duplicates(['Part Number'],keep='last')      # Only Keep the latest version of the part
    my_pfep = my_pfep.rename(columns={'Part Number':'Part_p','Part Name':'DESC','Mass Status (kg)':'weight'})
    
    
    ##SBOM
    sbom = read_sbom()
    sbom = sbom.rename(columns={'Extracted Part #':'Part_p','IMPLM_Name(R)':'Part_s','IMPLM_Title(R)':'Desc'})
    
    
    ##EBOM
    ebom = read_ebom()
    ebom.columns
    ebom = ebom.rename(columns={'Name':'Part_p','Title':'DESC'})
    
    
    a = sbom.merge(ebom,on=['Part_p'],how='left')
    # b = a.merge(full_bom[['NameCheck', 'SBOM Check', 'EBOM Order', 'Level', 'Part_p',
    #                      'Parent', 'DESC', 'Maturity State']],on=['Part_p'],how='left')
    c = a.merge(my_pfep[['Part_p', 'weight','Revision','Program','DESC','Level','System',
                         'Subsystem','Status','Bounding Box Length (mm)', 'Bounding Box Width (mm)',
                         'Bounding Box Height (mm)','Ship-From Country','Cost Status',
                         'Planning time fence','Planned Deliv. Time','GR Processing Time',
                         'Inbound Transit Days']],on=['Part_p'],how='left')
    
    # c.to_excel(r'C:\Arun\Projects\Forecast\Data_Analysis\my_fin.xlsx')
    
    df = c.copy()

    # (df.groupby(['SBOM Category','SBOM Subcategory'],as_index=False)['Part_s'].count()).ffill().to_clipboard()
    df['SBOM Category#'],df['SBOM Category'] = df['SBOM Category'].str.split(pat='-',expand=True).loc[:,0],df['SBOM Category'].str.split(pat='-',expand=True).loc[:,1]
    df['SBOM Subcategory#'],df['SBOM Subcategory'] = df['SBOM Subcategory'].str.split(pat='-',expand=True).loc[:,0],df['SBOM Subcategory'].str.split(pat='-',expand=True).loc[:,1]
    
    df = str_clean(df, cols=['Ship-From Country','SBOM Category','SBOM Subcategory'])

    ### Leadtimes
    df['lead_time'] = np.where(df['Ship-From Country'].isin(['US']),1,1.6) 
    
    ### Volume and weight
    df = num_clean(df,cols=['Cost Status','Bounding Box Length (mm)', 'Bounding Box Width (mm)',
                       'Bounding Box Height (mm)','weight'])
    df['volume'] = df['Bounding Box Length (mm)']*df['Bounding Box Width (mm)']*df['Bounding Box Height (mm)']/1000000000
        
    

    ### Part per vehical
    full_bom = read_full_ebom()
    full_bom = full_bom.rename(columns={'Name':'Part_p','Title':'DESC'})
    full_bom = full_bom[full_bom['Part_p'].isin(df['Part_p'])]
    full_bom['SBOM Check'] = pd.to_numeric(full_bom['SBOM Check'],errors='coerce')
    part_per_vehical = full_bom.groupby(['Part_p','DESC'],as_index=False)['SBOM Check'].sum()
    
    
    ### Vehicle Population
    vehicle_popu = read_vehicle_population()
    vehicle_popu = vehicle_popu.set_index('Vehicle')
    # vehicle_popu = vehicle_popu[vehicle_popu['Vehicle'].isin([vehicle_model])]
    vehicle_popu_new = pd.DataFrame()
    for i in vehicle_popu.index.unique():
        dummy = vehicle_popu.copy()
        dummy = dummy.loc[i,:]
        dummy.loc[:,'agg_popu'] = dummy.loc[:,'Qty'].expanding().sum()
        vehicle_popu_new = vehicle_popu_new.append(dummy)
    vehicle_popu_new = vehicle_popu_new.reset_index()
    
    
    ### Service impacting Facrots
    part_impact = read_seg_inputs()
    part_collision_rate = {1:0.06,
                           2:0.04,
                           3:0.03,
                           4:0.01}   #estimates collision impact on part associated with its vitality code
    part_impact['collision_rate'] =  part_impact['vitality_code'].map(part_collision_rate)
    part_impact = str_clean(part_impact,cols=['SBOM Category#'])
    part_impact = part_impact.drop_duplicates(subset ='SBOM Category#')

    
    
    ### We_Predict_Data
    we_pred = read_we_predict_data()
    we_predict_data_map = {"Steering":"Interior",
                            "Brakes":"Exterior",
                            "Suspension":"Exterior",
                            "Wheels and Tires":"Exterior",
                            "Thermal Management":"HVAC",
                            "High Voltage Battery":"Electrical",
                            "High Voltage Distribution":"Electrical",
                            "Front Drive Unit":"Powertrain",
                            "Frame":"Chassis",
                            "Body":"Body/Structure",
                            "Closures":"Exterior",
                            "Exterior Attachments":"Exterior",
                            "Interior":"Interior",
                            "Seats":"Interior",
                            "Occupant Protection":"Interior",
                            "Low Voltage Electrical":"Electrical",
                            "Lighting":"Electrical",
                            "Driver Assistance":"Interior",
                            "Noise Vibration and Harshness":"Exterior",
                            "Adventure Products":"Exterior",
                            "Supplemental Components":"Nothing"}
        
    ### Final_structure
    dfp = df.merge(part_per_vehical,on=['Part_p'],how='left')
    dfp['CategoryName'] = dfp['SBOM Category'].map(we_predict_data_map) 
    dfp = dfp.merge(we_pred,on='CategoryName',how='left')
    
    dfp = str_clean(dfp,cols = ['SBOM Category#'])
    dfp = dfp.merge(part_impact[['SBOM Category#','reliability_rate','vitality_code','collision_rate','oh_cost', 'int_rate']],on=['SBOM Category#'],how='left')
    dfp['SBOM Check'] = dfp['SBOM Check'].fillna(1)    
    dfp['reliability_rate'] = dfp['reliability_rate'].fillna(0.02)   #Setting default Values for missing data
    dfp['vitality_code'] = dfp['vitality_code'].fillna(4)           #Setting default Values for missing data
    dfp['collision_rate'] = dfp['collision_rate'].fillna(0.1)       #Setting default Values for missing data
    dfp['service_mean'] = dfp['service_mean'].fillna(10)            #Setting default Values for missing data
    dfp['int_rate'] = dfp['int_rate'].fillna(0.13)                  #Setting default Values for missing data
    dfp['oh_cost'] = dfp['oh_cost'].fillna(36)                     #Setting default Values for missing data
    dfp['tsl'] = 0.97                                   #Defaulting TSL to 97%
    dfp = dfp.drop_duplicates(subset=['Part_p','Program'])    
    dfp['Qty Per Vehicle'] = dfp['SBOM Check']
    dfp['Program'] = dfp['Program'].fillna('R1T')




### Forecast
fcst_all = pd.DataFrame()
for i in vehicle_popu_new.Vehicle.unique():
    fcst = forecast_basic(veh_parc = vehicle_popu_new,dfp =dfp,vehicle_model = i,popu_type='agg_popu')
    fcst['Program'] = i
    fcst_all = fcst_all.append(fcst) 
    
    
    
    
    mm = dfp.merge(fcst_all,on='Part_p',how='left')
    mm['price'] = mm['Cost Status'].fillna(35) # Setting missing Price to $35
    mm['stdDev']= (mm['stdDev']).fillna(0).astype(int)
    mm['avg_fcst']= (mm['avg_fcst']).fillna(0).astype(int)
    mm['lead_time']= (mm['lead_time']).astype(float)

    
    # mm['stdDev'] = mm['stdDev'].fillna(1)
    # mm['avg_fcst']= (mm['avg_fcst']).fillna(1)

    ### EOQ and SS
    #!!!
    mm['ss_simple'] = np.round(1.88*np.sqrt((mm['lead_time']*(mm['stdDev'])**2)+((mm['lead_time']*0.10)*mm['avg_fcst'])**2),0)
    ##SS = Z*sqrt(Avg LT x STDEV Demand)^2 + (LT STDEV x Avg Sales)^2
    mm['eoq_simple'] = np.round(np.sqrt(2*mm['avg_fcst']*12*mm['oh_cost']/mm['price']*mm['int_rate']),0)
    
    
    mm['Avg_Stock'] = mm['ss_simple']+(mm['eoq_simple'])/2
    mm['Max_Stock'] = mm['ss_simple']+mm['eoq_simple']
    
    
    
    cols_light =        ['Part_p',                   'Part_s',
                           'Desc',                  
             'Service Part Check',          'Covered in SBOM',
                  'SBOM Category',         'SBOM Subcategory',
                 'SBOM Category#',        'SBOM Subcategory#',                                                         
                         'Status', 'Bounding Box Length (mm)',
        'Bounding Box Width (mm)', 'Bounding Box Height (mm)',
              'Ship-From Country',      'Planning time fence',
            'Planned Deliv. Time',     'Inbound Transit Days',
             'GR Processing Time',                'lead_time', 
                         'volume',                   'weight',
                           'DESC',               'SBOM Check',
                   'CategoryName',             'service_mean',
                    'service_min',              'service_max',
               'reliability_rate',            'vitality_code',
                 'collision_rate',                  'oh_cost',
                       'int_rate',                      'tsl',
                         'stdDev',                 'avg_fcst',
                          'price',                'ss_simple', 
                     'eoq_simple',                'Avg_Stock',
                     'Max_Stock','fcst_value','ss_value','eoq_value','avg_value','max_value'] 
    
    
    mm['fcst_value'] = mm['avg_fcst']*mm['price']
    mm['ss_value'] = mm['ss_simple']*mm['price']
    mm['eoq_value'] = mm['eoq_simple']*mm['price']
    mm['avg_value'] = mm['Avg_Stock']*mm['price']
    mm['max_value'] = mm['Max_Stock']*mm['price']

    mm_light = mm[cols_light].copy()
    today = dt.today().strftime('%m.%d.%Y')
    mm_light.to_excel(r'C:\Arun\Projects\Forecast\Data_Analysis\R1T_Initial_Demand_'+today+'.xlsx',index=False)
    
    
    mm.to_excel(r'C:\Arun\Projects\Forecast\Data_Analysis\R1T_Initial_Demand_'+today+'.xlsx',index=False)



    





#%%         
'''

    ### Complex SS and EOQ
    ss,EEoq = GetEOQSS_SaP_Iteration(mm.tsl,mm.lead_time,mm.stdDev,
                            mm.int_rate,mm.oh_cost,
                            mm.price,mm.avg_fcst,
                            GetSS_Serv2SAP)                    


def GetEOQSS_SaP_Iteration( tsl, LT, stdDev,
                            int_rate, oh_cost, price, eod,
                            ss_method):
    """
    Parameters
    ----------
    - tsl: Service you want
    - LT: Leadtime in months
    - stdDev: Monthly standard deviation
    - int_rate: Interest rate
    - oh_cost: Order handling cost
    - price: Price
    - eod: Estimate of demand (forecast)
    - ss_method: GetSS_Serv2SaP or GetSS_Serv2_v2

    Notes
    -----
    Implementation of the SAP hybrid with EOQ optimization.

    Returns two objects... SS and EOQ
    """
    # tsl = tsl.values
    # LT = LT.values
    # stdDev = stdDev.values
    # int_rate = int_rate.values
    # oh_cost = oh_cost.values
    # price = price.values
    # eod = eod.values
    # divisor is used to set the increment
    # can be edited to make the least amount
    # of iterations
    divisor = np.zeros(len(LT))
    divisor[:] = 3.0
    counter = 0
    EOQ_Increment = np.round(eod / divisor)
    EOQ_Increment[EOQ_Increment < 1] = 1
    # Calculate Wilson eoq... This will be the minumum
    # oq, and is applied in the end of this function
    EOQ = np.round(ne.evaluate(
        "sqrt(2 * eod * 12 * oh_cost / (int_rate * price))"), 3)
    EOQ[EOQ < 1] = 1
    # then the oq that we will test against
    OQ = np.zeros(len(LT), dtype=float)
    OQ[:] = 1.0
    OQPrev = np.zeros(len(LT))
    SS = ss_method(tsl, OQ, LT, stdDev)
    TotCost = ne.evaluate('(SS + OQ/2) * int_rate * price + ' +
                          'eod * 12 / OQ * oh_cost')
    TotCostPrev = TotCost.copy()
    Finished = np.zeros(len(LT))
    # loop maximum of x times, this so we avoid looping forever...
    while counter < 1000:
        # find parts where we are finished
        inx = ((TotCostPrev < TotCost) &
                (EOQ_Increment == 1) &
                (Finished == 0))
        if np.sum(inx) > 0:
            Finished[inx] = 1
            OQ[inx] -= EOQ_Increment[inx]
        inx = ((TotCost > TotCostPrev) &
                (EOQ_Increment > 1) &
                (Finished == 0))
        if np.sum(inx) > 0:
            OQ[inx] = OQ[inx] - EOQ_Increment[inx] * 2
            OQ[inx & (OQ < 1)] = 1.0
            SS[inx] = ss_method(tsl[inx], OQ[inx],
                                LT[inx], stdDev[inx])
            TotCost[inx] = ((SS[inx] + OQ[inx] / 2) * int_rate[inx] * price[inx] +
                            eod[inx] * 12 / OQ[inx] * oh_cost[inx])
            divisor[inx] = divisor[inx] * 5
            EOQ_Increment[inx] = np.round(eod[inx] / divisor[inx])
            EOQ_Increment[EOQ_Increment < 1] = 1

        inx = (OQ != OQPrev) & (Finished == 0)
        if np.sum(inx) == 0:
            break
        TotCostPrev[inx] = TotCost[inx]
        OQPrev[inx] = OQ[inx]
        OQ[inx] += EOQ_Increment[inx]
        # update new SS
        SS[inx] = ss_method(tsl[inx], OQ[inx],
                            LT[inx], stdDev[inx])
        TotCost[inx] = ((SS[inx] + OQ[inx] / 2) * int_rate[inx] * price[inx] +
                        eod[inx] * 12 / OQ[inx] * oh_cost[inx])

        counter += 1
    if counter == 1000:
        print('counter in GetEOQSS_Serv2SaP reached 1000 times')
    inx = OQ < np.round(EOQ)
    if np.sum(inx) > 0:
        OQ[inx] = EOQ[inx]
        SS[inx] = ss_method(tsl[inx], OQ[inx],
                            LT[inx], stdDev[inx])
    return np.round(SS, 3), np.round(OQ, 3)

def GetSS_Serv2SAP(tsl, OQ, LT, stdDev):
    """
    implementation of a SaP formula

    Parameters
    ----------
    - tsl: Service you want
    - OQ: Orderquantity
    - LT: Leadtime in months
    - stdDev: Monthly standard deviation
    """
    servicef = (
        ne.evaluate("OQ * (1 - tsl) / (stdDev * sqrt(LT))"))
    servicef[servicef <= 0] = 0
    servicef[servicef > 1] = 0.9999
    a1 = 2.51552
    a2 = 0.80285
    a3 = 0.01038
    a4 = 1.43279
    a5 = 0.18927
    a6 = 0.00131
    lv_h1 = ne.evaluate("sqrt(-2 * log(servicef))")
    lv_h2 = ne.evaluate("a1 + a2 * lv_h1 + a3 * lv_h1 * lv_h1")
    lv_h3 = ne.evaluate("1 + a4 * lv_h1 + a5 * lv_h1 * lv_h1 + a6 * lv_h1 * lv_h1 * lv_h1")
    ev_z = ne.evaluate("lv_h1 - lv_h2 / lv_h3")
    SS = np.round(ne.evaluate("ev_z * sqrt(LT) * stdDev"), 3)
    SS[SS < 0] = 0
    return SS

def GetSS_Serv2_v2(self, tsl, OQ, LT, stdDev):
    """
    implementation of a Stig-arne Mattsons formula

    Parameters
    ----------
    - tsl: Service you want
    - OQ: Orderquantity
    - LT: Leadtime in months
    - stdDev: Monthly standard deviation
    """
    servicef = (
        ne.evaluate("OQ * (1 - tsl) / (stdDev * sqrt(LT))"))
    if np.array(servicef).ndim != 0:
        servicef[servicef > 0.39] = 0.39
        servicef[servicef < 0.0001] = 0.0001
    else:
        if servicef > 0.39: servicef = 0.39
        if servicef < 0.0001: servicef = 0.0001
    k = self.getk(servicef)
    return ne.evaluate("k * sqrt(LT) * stdDev")
'''